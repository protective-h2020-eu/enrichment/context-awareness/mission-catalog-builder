# MissionCatalogBuilder

This project was generated with [Angular CLI](https://github.com/angular/angular-cli) version 1.6.6.

## How to run

Run `ng serve --host 0.0.0.0` on the command line. Navigate to `http://localhost:4200/`. The app will automatically reload if you change any of the source files. This will also be accessible from other PCs on the network.

## Instructions
1. Download the CSV templates by clicking the "Download CSV Template Files"
2. Fill-in the files with all the details of your system.
3. When saving the files in Excel, Make sure to save as CSV files.
4. Submit the files one-at-a-time using the file submit inputs provided.
5. Once all files are submitted, the CDG output will be printed to the screen.
6. Fill-in a name for the CDG file. You can then download the CDG file.
7. This can then be uploaded to the ProtDash upload page.

## Run Docker Container
Use the docker-compose file provided as well as the image located at: `registry.gitlab.com/protective-h2020-eu/enrichment/context-awareness/mission-catalog-builder:v1`
```
docker-compose up -d
```
