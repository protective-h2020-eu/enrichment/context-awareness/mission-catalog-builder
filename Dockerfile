FROM johnpapa/angular-cli:latest

RUN mkdir ./mission-catalog-builder
COPY ./ ./mission-catalog-builder/

WORKDIR ./mission-catalog-builder
RUN mkdir node_modules
