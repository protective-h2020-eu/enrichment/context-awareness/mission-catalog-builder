import { CommonModule } from '@angular/common';
import { HttpClientModule } from '@angular/common/http';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { PapaParseModule } from 'ngx-papaparse';
import { PageHeaderModule } from '../../shared/modules/page-header/page-header.module';
import { CsvFileReadComponent } from './csv-file-read.component';

@NgModule({
  imports: [
    CommonModule,
    PageHeaderModule,
    FormsModule,
    ReactiveFormsModule,
    HttpClientModule,
    PapaParseModule,
  ],
  declarations: [CsvFileReadComponent],
  exports: [CsvFileReadComponent]
})
export class CsvFileReadModule {}
