
export interface CDGS {
  cdg: Cdg;
}

export interface Cdg {
  name: 'missionLayer' | 'applicationLayer' | 'infrastructureLayer' | 'operationalLayer';
  its?: It[];
  ild?: Ild[];
  sw?: Node[];
  node?: Node[];
  m?: M[];
  bp?: Bp[];
  ia?: Ia[];
}

export interface Bp {
  id: string;
  bpdep?: M[];
  iadep?: M[];
}

export interface M {
  id: string;
  weight?: Weight;
  mdep?: M[];
}

export enum Weight {
  critical = 'critical',
  high = 'high',
  low = 'low',
  medium = 'medium'
}

export interface Ia {
  id: string;
  iadep?: M[];
}

export interface Ild {
  fromVertexId: string;
  toVertexId: string;
  toCdg: ToCdg;
  weight: Weight;
}

export enum ToCdg {
  MissionLayer = 'missionLayer',
  ApplicationLayer = 'applicationLayer',
  InfrastructureLayer = 'infrastructureLayer',
  OperationalLayer = 'operationalLayer'
}

export interface It {
  id: string;
  itsdep?: M[];
}

export interface Node {
  id: string;
  netdep?: M[];
  swdep?: M[];
}
