export interface AssetProfile {
  'Asset Id': string;
  Name: string;
  Type: string;
  'Business Owner': string;
  'Technical Owner': string;
  'Supported Service Id': string;
  'Supported Service Weight': string;
  'Supported Service Type': string;
  'Supported Asset Id': string;
  'Supported Asset Weight': string;
  'Supported Asset Type': string;
}
