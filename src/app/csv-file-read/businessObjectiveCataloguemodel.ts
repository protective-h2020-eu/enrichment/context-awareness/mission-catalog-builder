export interface BusinessObjectiveCatalogue {
  'Business Objective': string;
  'Business Owner': string;
  Description: string;
  'Type': string;
  Weighting: string;
  'Supported Service Id': string;
  'Supported Service Weight': string;
  'Supported Service Type': string;
  'Supported Asset Id': string;
  'Supported Asset Weight': string;
  'Supported Asset Type': string;
}
