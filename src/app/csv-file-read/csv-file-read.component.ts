import { HttpClient } from '@angular/common/http';
import { Component, OnInit, ViewChild } from '@angular/core';
import { FormControl, FormGroup, NgForm, Validators } from '@angular/forms';
import * as FileSaver from 'file-saver';
import { PapaParseService } from 'ngx-papaparse';
import { slideToBottom } from '../router.animations';
import { AssetProfile } from './assetProfileCatalogue';
import { BusinessObjectiveCatalogue } from './businessObjectiveCataloguemodel';
import { Bp, CDGS, Ia, Ild, It, M, Node, ToCdg, Weight } from './cdgModel';
import { ServiceProfile } from './serviceProfileCatalogue';

@Component({
  selector: 'app-csv-read',
  templateUrl: './csv-file-read.component.html',
  styleUrls: ['./csv-file-read.component.css'],
  animations: [slideToBottom()]
})
export class CsvFileReadComponent implements OnInit {
  statusCreateForm: FormGroup;
  fileDescription: FormControl;
  fileToUpload: File = null;
  fileStringBo: string;
  fileStringAp: string;
  fileStringSp: string;
  boFilePath: string;
  apFilePath: string;
  spFilePath: string;
  currentFileType: 'bo' | 'ap' | 'sp';
  currentCsvData: string;
  businessObjectiveData: BusinessObjectiveCatalogue[];
  assetProfileData: AssetProfile[];
  serviceProfileData: ServiceProfile[];
  jsonOutput: string;
  cdgArray: CDGS[];
  newCdg: CDGS;
  fileForDownload: string;
  uploadProgress: number;
  uploadPercent: string;
  uploadComplete = false;
  uploadingProgressing = false;
  fileUploadSub: any;
  serverResponse: any;
  errorArray: any;

  @ViewChild('boInput') boFileInput: any;
  @ViewChild('apInput') apFileInput: any;
  @ViewChild('spInput') spFileInput: any;

  constructor(private http: HttpClient, private papa: PapaParseService) {}

  ngOnInit() {
    /* initilize the form and/or extra form fields
                Do not initialize the file field
            */

    this.fileDescription = new FormControl('', [
      Validators.required,
      Validators.minLength(4),
      Validators.maxLength(280)
    ]);
    this.statusCreateForm = new FormGroup({
      description: this.fileDescription
    });
    this.fileStringBo = 'Choose CSV File';
    this.fileStringAp = 'Choose CSV File';
    this.fileStringSp = 'Choose CSV File';
  }
  handleSubmit(event: any, statusNgForm: NgForm, statusFormGroup: FormGroup) {
    console.log('statusNgForm: ', statusNgForm.status);
    event.preventDefault();

    if (statusNgForm.submitted) {
      this.readFile(this.fileToUpload);
    }
  }
  readFile(file: File) {
    console.log('Name of file: ', file.name);
    const reader = new FileReader();
    reader.onload = () => {
      this.currentCsvData = reader.result.toString();
      this.papa.parse(this.currentCsvData, {
        header: true,
        complete: result => {
          if (this.currentFileType === 'bo') {
            this.businessObjectiveData = result.data;
            console.log(this.businessObjectiveData);
          } else if (this.currentFileType === 'ap') {
            this.assetProfileData = result.data;
            console.log(this.assetProfileData);
          } else {
            this.serviceProfileData = result.data;
            console.log(this.serviceProfileData);
            this.constructCdg();
          }

          this.jsonOutput = result.data;
        }
      });
    };
    reader.readAsText(file);
  }
  handleBoFileInput(files: FileList) {
    this.fileStringBo = files.item(0).name;
    const fileItem = files.item(0);
    this.boFileInput = fileItem.name;
    this.currentFileType = 'bo';
    this.fileToUpload = fileItem;
  }
  handleApFileInput(files: FileList) {
    this.fileStringAp = files.item(0).name;
    const fileItem = files.item(0);
    this.apFileInput = fileItem.name;
    this.currentFileType = 'ap';
    this.fileToUpload = fileItem;
  }
  handleSpFileInput(files: FileList) {
    this.fileStringSp = files.item(0).name;
    const fileItem = files.item(0);
    this.spFileInput = fileItem.name;
    this.currentFileType = 'sp';
    this.fileToUpload = fileItem;
  }
  constructCdg() {
    /* construct mission layer. */
    this.constructApplicationLayer();
    this.constructInfrastructureLayer();
    this.constructMissionLayer();
    this.constructOperationalLayer();
  }
  getLayer(type: string) {
    if (type === 'Information Asset' || type === 'Business Process') {
      return 'OperationalLayer';
    } else if (type === 'IT Service') {
      return 'ApplicationLayer';
    } else if (type === 'Mission') {
      return 'MissionLayer';
    } else {
      return 'InfrastructureLayer';
    }
  }

  constructMissionLayer() {
    /* construct mission layer. */
    const cdgMission: CDGS = <CDGS>{
      cdg: {
        name: 'missionLayer'
      }
    };

    this.businessObjectiveData.forEach(elem => {
      if (cdgMission.cdg.m === undefined) {
        const mArray: M[] = [];
        cdgMission.cdg.m = mArray;
      }
      // If mission id already present do not add it to the array
      if (
        cdgMission.cdg.m.findIndex(x => x.id === elem['Business Objective']) ===
        -1
      ) {
        cdgMission.cdg.m.push({
          id: elem['Business Objective'],
          weight: Weight[elem.Weighting]
        });
      }
    });
    /// End of mission layer construction ///

    if (this.cdgArray === undefined) {
      this.cdgArray = [];
    }
    this.cdgArray.push(cdgMission);

    this.addMissionLayerInterDeps(cdgMission);
    this.addMissionLayerIntraDeps(cdgMission);
  }
  addMissionLayerIntraDeps(cdgMission: CDGS) {
    cdgMission.cdg.m.forEach(elem => {
      if (elem.mdep === undefined) {
        const mdepArray: M[] = [];
        elem.mdep = mdepArray;
      }

      this.businessObjectiveData.forEach(line => {
        if (
          line['Business Objective'] === elem.id &&
          line['Supported Asset Id'] !== '' &&
          this.getLayer(line['Supported Asset Type']) === 'MissionLayer' &&
          line['Supported Asset Type'] === 'Mission'
        ) {
          elem.mdep.push({
            id: line['Supported Asset Id'],
            weight: Weight[line['Supported Asset Weight']]
          });
        }
      });
      if (elem.mdep.length < 1) {
        delete elem.mdep;
      }
    });
  }
  addMissionLayerInterDeps(cdgMission) {
    /* Inter-layer dependencies of the mission layer */
    this.businessObjectiveData.forEach(elem => {
      if (cdgMission.cdg.ild === undefined) {
        const ildArray: Ild[] = [];
        cdgMission.cdg.ild = ildArray;
      }

      if (elem['Supported Asset Id'] !== '') {
        const layer = this.getLayer(elem['Supported Asset Type']);
        if (layer !== 'MissionLayer') {
          cdgMission.cdg.ild.push({
            fromVertexId: elem['Business Objective'],
            toVertexId: elem['Supported Asset Id'],
            toCdg: ToCdg[layer],
            weight: Weight[elem['Supported Asset Weight']]
          });
        }
      }

      if (elem['Supported Service Id'] !== '') {
        const layer = this.getLayer(elem['Supported Service Type']);
        if (layer !== 'MissionLayer') {
          cdgMission.cdg.ild.push({
            fromVertexId: elem['Business Objective'],
            toVertexId: elem['Supported Service Id'],
            toCdg: ToCdg[layer],
            weight: Weight[elem['Supported Service Weight']]
          });
        }
      }
    });
    if (cdgMission.cdg.ild.length < 1) {
      delete cdgMission.cdg.ild;
    }
  }
  constructOperationalLayer() {
    const cdgOperation: CDGS = <CDGS>{
      cdg: {
        name: 'operationalLayer'
      }
    };

    this.assetProfileData.forEach(elem => {
      if (cdgOperation.cdg.bp === undefined) {
        const bpArray: Bp[] = [];
        cdgOperation.cdg.bp = bpArray;
      }

      // Add only unique assets to bp array
      if (
        cdgOperation.cdg.bp.findIndex(x => x.id === elem['Asset Id']) === -1
      ) {
        // Add business processes to the correct array
        if (
          this.getLayer(elem.Type) === 'OperationalLayer' &&
          elem.Type === 'Business Process'
        ) {
          cdgOperation.cdg.bp.push({
            id: elem['Asset Id']
          });
        }
      }

      if (cdgOperation.cdg.ia === undefined) {
        const iaArray: Ia[] = [];
        cdgOperation.cdg.ia = iaArray;
      }

      // Add only unique assets to ia array
      if (
        cdgOperation.cdg.ia.findIndex(x => x.id === elem['Asset Id']) === -1
      ) {
        // Add information assets to the correct array
        if (
          this.getLayer(elem.Type) === 'OperationalLayer' &&
          elem.Type === 'Information Asset'
        ) {
          cdgOperation.cdg.ia.push({
            id: elem['Asset Id']
          });
        }
      }
    });

    this.serviceProfileData.forEach(elem => {
      if (cdgOperation.cdg.bp === undefined) {
        const bpArray: Bp[] = [];
        cdgOperation.cdg.bp = bpArray;
      }

      // Add only unique services to bp array
      if (
        cdgOperation.cdg.bp.findIndex(x => x.id === elem['Service Id']) === -1
      ) {
        // Add business processes to the correct array
        if (
          this.getLayer(elem.Type) === 'OperationalLayer' &&
          elem.Type === 'Business Process'
        ) {
          cdgOperation.cdg.bp.push({
            id: elem['Service Id']
          });
        }
      }

      if (cdgOperation.cdg.ia === undefined) {
        const iaArray: Ia[] = [];
        cdgOperation.cdg.ia = iaArray;
      }
      // Add only unique services to ia array
      if (
        cdgOperation.cdg.ia.findIndex(x => x.id === elem['Service Id']) === -1
      ) {
        // Add information assets to the correct array
        if (
          this.getLayer(elem.Type) === 'OperationalLayer' &&
          elem.Type === 'Information Asset'
        ) {
          cdgOperation.cdg.ia.push({
            id: elem['Service Id']
          });
        }
      }

      // Add intra-layer dependencies for the Operational Layer
    });

    console.log('cdgOperation ia length: ', cdgOperation.cdg.ia.length);

    if (this.cdgArray === undefined) {
      this.cdgArray = [];
    }
    this.cdgArray.push(cdgOperation);

    this.addOperationLayerIntraDeps(cdgOperation);
    this.addOperationalLayerInterDeps(cdgOperation);
  }
  // bp can have bpdep and iadep. ia can only have iadep
  addOperationLayerIntraDeps(cdgOperation: CDGS) {
    cdgOperation.cdg.bp.forEach(elem => {
      if (elem.bpdep === undefined) {
        const bpdepArray: M[] = [];
        elem.bpdep = bpdepArray;
      }
      if (elem.iadep === undefined) {
        const iadepArray: M[] = [];
        elem.iadep = iadepArray;
      }

      this.serviceProfileData.forEach(line => {
        if (
          line['Service Id'] === elem.id &&
          line['Supported Asset Id'] !== '' &&
          this.getLayer(line['Supported Asset Type']) === 'OperationalLayer' &&
          line['Supported Asset Type'] === 'Information Asset'
        ) {
          elem.iadep.push({
            id: line['Supported Asset Id'],
            weight: Weight[line['Supported Asset Weight']]
          });
        }

        if (
          line['Service Id'] === elem.id &&
          line['Supported Service Id'] !== '' &&
          this.getLayer(line['Supported Service Type']) ===
            'OperationalLayer' &&
          line['Supported Service Type'] === 'Business Process'
        ) {
          elem.bpdep.push({
            id: line['Supported Service Id'],
            weight: Weight[line['Supported Service Weight']]
          });
        }
      });
      if (elem.bpdep.length < 1) {
        delete elem.bpdep;
      }
      if (elem.iadep.length < 1) {
        delete elem.iadep;
      }
    });

    cdgOperation.cdg.ia.forEach(elem => {
      if (elem.iadep === undefined) {
        const iadepArray: M[] = [];
        elem.iadep = iadepArray;
      }

      this.assetProfileData.forEach(line => {
        if (
          line['Asset Id'] === elem.id &&
          line['Supported Asset Id'] !== '' &&
          this.getLayer(line['Supported Asset Type']) === 'OperationalLayer' &&
          line['Supported Asset Type'] === 'Information Asset'
        ) {
          elem.iadep.push({
            id: line['Supported Asset Id'],
            weight: Weight[line['Supported Asset Weight']]
          });
        }
      });
      if (elem.iadep.length < 1) {
        delete elem.iadep;
      }
    });
  }
  addOperationalLayerInterDeps(cdgOperation) {
    this.assetProfileData.forEach(elem => {
      if (cdgOperation.cdg.ild === undefined) {
        const ildArray: Ild[] = [];
        cdgOperation.cdg.ild = ildArray;
      }
      if (elem['Supported Asset Id'] !== '') {
        const fromLayer = this.getLayer(elem['Type']);
        const toLayer = this.getLayer(elem['Supported Asset Type']);
        console.log('toLayer', toLayer);

        if (
          fromLayer === 'OperationalLayer' &&
          toLayer !== 'OperationalLayer'
        ) {
          cdgOperation.cdg.ild.push({
            fromVertexId: elem['Asset Id'],
            toVertexId: elem['Supported Asset Id'],
            toCdg: ToCdg[toLayer],
            weight: Weight[elem['Supported Asset Weight']]
          });
        }
      }

      if (elem['Supported Service Id'] !== '') {
        const fromLayer = this.getLayer(elem['Type']);
        const toLayer = this.getLayer(elem['Supported Service Type']);
        console.log('toLayer', toLayer);

        if (
          fromLayer === 'OperationalLayer' &&
          toLayer !== 'OperationalLayer'
        ) {
          cdgOperation.cdg.ild.push({
            fromVertexId: elem['Asset Id'],
            toVertexId: elem['Supported Service Id'],
            toCdg: ToCdg[toLayer],
            weight: Weight[elem['Supported Service Weight']]
          });
        }
      }
    });

    this.serviceProfileData.forEach(elem => {
      if (cdgOperation.cdg.ild === undefined) {
        const ildArray: Ild[] = [];
        cdgOperation.cdg.ild = ildArray;
      }
      if (elem['Supported Asset Id'] !== '') {
        const fromLayer = this.getLayer(elem['Type']);
        const toLayer = this.getLayer(elem['Supported Asset Type']);
        console.log('toLayer', toLayer);

        if (
          fromLayer === 'OperationalLayer' &&
          toLayer !== 'OperationalLayer'
        ) {
          cdgOperation.cdg.ild.push({
            fromVertexId: elem['Service Id'],
            toVertexId: elem['Supported Asset Id'],
            toCdg: ToCdg[toLayer],
            weight: Weight[elem['Supported Asset Weight']]
          });
        }
      }

      if (elem['Supported Service Id'] !== '') {
        const fromLayer = this.getLayer(elem['Type']);
        const toLayer = this.getLayer(elem['Supported Service Type']);
        console.log('toLayer', toLayer);
        if (
          fromLayer === 'OperationalLayer' &&
          toLayer !== 'OperationalLayer'
        ) {
          cdgOperation.cdg.ild.push({
            fromVertexId: elem['Service Id'],
            toVertexId: elem['Supported Service Id'],
            toCdg: ToCdg[toLayer],
            weight: Weight[elem['Supported Service Weight']]
          });
        }
      }
    });
    if (cdgOperation.cdg.ild.length < 1) {
      delete cdgOperation.cdg.ild;
    }
  }
  constructInfrastructureLayer() {
    const cdgInfrastructure: CDGS = <CDGS>{
      cdg: {
        name: 'infrastructureLayer'
      }
    };

    this.assetProfileData.forEach(elem => {
      if (cdgInfrastructure.cdg.node === undefined) {
        const nodeArray: Node[] = [];
        cdgInfrastructure.cdg.node = nodeArray;
      }

      // Add only unique assets to bp array
      if (
        cdgInfrastructure.cdg.node.findIndex(x => x.id === elem['Asset Id']) ===
        -1
      ) {
        // Add business processes to the correct array
        if (
          this.getLayer(elem.Type) === 'InfrastructureLayer' &&
          elem.Type === 'Node'
        ) {
          cdgInfrastructure.cdg.node.push({
            id: elem['Asset Id']
          });
        }
      }

      if (cdgInfrastructure.cdg.sw === undefined) {
        const swArray: Node[] = [];
        cdgInfrastructure.cdg.sw = swArray;
      }

      // Add only unique assets to ia array
      if (
        cdgInfrastructure.cdg.sw.findIndex(x => x.id === elem['Asset Id']) ===
        -1
      ) {
        // Add information assets to the correct array
        if (
          this.getLayer(elem.Type) === 'InfrastructureLayer' &&
          elem.Type === 'Software'
        ) {
          cdgInfrastructure.cdg.sw.push({
            id: elem['Asset Id']
          });
        }
      }
    });

    this.serviceProfileData.forEach(elem => {
      if (cdgInfrastructure.cdg.node === undefined) {
        const nodeArray: Node[] = [];
        cdgInfrastructure.cdg.node = nodeArray;
      }

      // Add only unique services to bp array
      if (
        cdgInfrastructure.cdg.node.findIndex(
          x => x.id === elem['Service Id']
        ) === -1
      ) {
        // Add business processes to the correct array
        if (
          this.getLayer(elem.Type) === 'InfrastructureLayer' &&
          elem.Type === 'Node'
        ) {
          cdgInfrastructure.cdg.node.push({
            id: elem['Service Id']
          });
        }
      }

      if (cdgInfrastructure.cdg.sw === undefined) {
        const swArray: Node[] = [];
        cdgInfrastructure.cdg.sw = swArray;
      }
      // Add only unique services to ia array
      if (
        cdgInfrastructure.cdg.sw.findIndex(x => x.id === elem['Service Id']) ===
        -1
      ) {
        // Add information assets to the correct array
        if (
          this.getLayer(elem.Type) === 'InfrastructureLayer' &&
          elem.Type === 'Software'
        ) {
          cdgInfrastructure.cdg.sw.push({
            id: elem['Service Id']
          });
        }
      }

      // Add intra-layer dependencies for the Operational Layer
    });

    if (this.cdgArray === undefined) {
      this.cdgArray = [];
    }
    this.cdgArray.push(cdgInfrastructure);

    this.addInfrastructureLayerIntraDeps(cdgInfrastructure);
    this.addInfrastructureLayerInterDeps(cdgInfrastructure);
  }
  addInfrastructureLayerIntraDeps(cdgInfrastructure: CDGS) {
    cdgInfrastructure.cdg.node.forEach(elem => {
      if (elem.netdep === undefined) {
        const netdepArray: M[] = [];
        elem.netdep = netdepArray;
      }
      this.assetProfileData.forEach(line => {
        if (
          line['Asset Id'] === elem.id &&
          line['Supported Asset Id'] !== '' &&
          this.getLayer(line['Supported Asset Type']) ===
            'InfrastructureLayer' &&
          line['Supported Asset Type'] === 'Node'
        ) {
          elem.netdep.push({
            id: line['Supported Asset Id'],
            weight: Weight[line['Supported Asset Weight']]
          });
        }
      });
      if (elem.netdep.length < 1) {
        delete elem.netdep;
      }
    });

    cdgInfrastructure.cdg.sw.forEach(elem => {
      if (elem.swdep === undefined) {
        const swdepArray: M[] = [];
        elem.swdep = swdepArray;
      }
      if (elem.netdep === undefined) {
        const netdepArray: M[] = [];
        elem.netdep = netdepArray;
      }

      this.assetProfileData.forEach(line => {
        if (
          line['Asset Id'] === elem.id &&
          line['Supported Asset Id'] !== '' &&
          this.getLayer(line['Supported Asset Type']) ===
            'InfrastructureLayer' &&
          line['Supported Asset Type'] === 'Software'
        ) {
          elem.swdep.push({
            id: line['Supported Asset Id'],
            weight: Weight[line['Supported Asset Weight']]
          });
        }
        if (
          line['Asset Id'] === elem.id &&
          line['Supported Asset Id'] !== '' &&
          this.getLayer(line['Supported Asset Type']) ===
            'InfrastructureLayer' &&
          line['Supported Asset Type'] === 'Node'
        ) {
          elem.netdep.push({
            id: line['Supported Asset Id'],
            weight: Weight[line['Supported Asset Weight']]
          });
        }
      });

      this.serviceProfileData.forEach(line => {
        if (
          line['Service Id'] === elem.id &&
          line['Supported Asset Id'] !== '' &&
          this.getLayer(line['Supported Asset Type']) ===
            'InfrastructureLayer' &&
          line['Supported Asset Type'] === 'Software'
        ) {
          elem.swdep.push({
            id: line['Supported Asset Id'],
            weight: Weight[line['Supported Asset Weight']]
          });
        }
        if (
          line['Service Id'] === elem.id &&
          line['Supported Asset Id'] !== '' &&
          this.getLayer(line['Supported Asset Type']) ===
            'InfrastructureLayer' &&
          line['Supported Asset Type'] === 'Node'
        ) {
          elem.netdep.push({
            id: line['Supported Asset Id'],
            weight: Weight[line['Supported Asset Weight']]
          });
        }
      });
      if (elem.swdep.length < 1) {
        delete elem.swdep;
      }
      if (elem.netdep.length < 1) {
        delete elem.netdep;
      }
    });
  }

  addInfrastructureLayerInterDeps(cdgInfrastructure: CDGS) {
    this.assetProfileData.forEach(elem => {
      if (cdgInfrastructure.cdg.ild === undefined) {
        const ildArray: Ild[] = [];
        cdgInfrastructure.cdg.ild = ildArray;
      }
      if (elem['Supported Asset Id'] !== '') {
        const fromLayer = this.getLayer(elem.Type);
        const toLayer = this.getLayer(elem['Supported Asset Type']);
        if (
          fromLayer === 'InfrastructureLayer' &&
          toLayer !== 'InfrastructureLayer'
        ) {
          cdgInfrastructure.cdg.ild.push({
            fromVertexId: elem['Asset Id'],
            toVertexId: elem['Supported Asset Id'],
            toCdg: ToCdg[toLayer],
            weight: Weight[elem['Supported Asset Weight']]
          });
        }
      }

      if (elem['Supported Service Id'] !== '') {
        const fromLayer = this.getLayer(elem.Type);
        const toLayer = this.getLayer(elem['Supported Service Type']);
        if (
          fromLayer === 'InfrastructureLayer' &&
          toLayer !== 'InfrastructureLayer'
        ) {
          cdgInfrastructure.cdg.ild.push({
            fromVertexId: elem['Asset Id'],
            toVertexId: elem['Supported Service Id'],
            toCdg: ToCdg[toLayer],
            weight: Weight[elem['Supported Service Weight']]
          });
        }
      }
    });

    this.serviceProfileData.forEach(elem => {
      if (cdgInfrastructure.cdg.ild === undefined) {
        const ildArray: Ild[] = [];
        cdgInfrastructure.cdg.ild = ildArray;
      }
      if (elem['Supported Asset Id'] !== '') {
        const fromLayer = this.getLayer(elem.Type);
        const toLayer = this.getLayer(elem['Supported Asset Type']);
        if (
          fromLayer === 'InfrastructureLayer' &&
          toLayer !== 'InfrastructureLayer'
        ) {
          cdgInfrastructure.cdg.ild.push({
            fromVertexId: elem['Service Id'],
            toVertexId: elem['Supported Asset Id'],
            toCdg: ToCdg[toLayer],
            weight: Weight[elem['Supported Asset Weight']]
          });
        }
      }

      if (elem['Supported Service Id'] !== '') {
        const fromLayer = this.getLayer(elem.Type);
        const toLayer = this.getLayer(elem['Supported Service Type']);
        if (
          fromLayer === 'InfrastructureLayer' &&
          toLayer !== 'InfrastructureLayer'
        ) {
          cdgInfrastructure.cdg.ild.push({
            fromVertexId: elem['Service Id'],
            toVertexId: elem['Supported Service Id'],
            toCdg: ToCdg[toLayer],
            weight: Weight[elem['Supported Service Weight']]
          });
        }
      }
    });
    if (cdgInfrastructure.cdg.ild.length < 1) {
      delete cdgInfrastructure.cdg.ild;
    }
  }
  constructApplicationLayer() {
    const cdgApplication: CDGS = <CDGS>{
      cdg: {
        name: 'applicationLayer'
      }
    };

    this.assetProfileData.forEach(elem => {
      if (cdgApplication.cdg.its === undefined) {
        const itsArray: It[] = [];
        cdgApplication.cdg.its = itsArray;
      }

      // Add only unique assets to its array
      if (
        cdgApplication.cdg.its.findIndex(x => x.id === elem['Asset Id']) === -1
      ) {
        // Add IT Services to the correct array
        if (
          this.getLayer(elem.Type) === 'ApplicationLayer' &&
          elem.Type === 'IT Service'
        ) {
          cdgApplication.cdg.its.push({
            id: elem['Asset Id']
          });
        }
      }
    });

    this.serviceProfileData.forEach(elem => {
      if (cdgApplication.cdg.its === undefined) {
        const itsArray: It[] = [];
        cdgApplication.cdg.its = itsArray;
      }

      // Add only unique services to bp array
      if (
        cdgApplication.cdg.its.findIndex(x => x.id === elem['Service Id']) ===
        -1
      ) {
        // Add IT Services to the correct array
        if (
          this.getLayer(elem.Type) === 'ApplicationLayer' &&
          elem.Type === 'IT Service'
        ) {
          cdgApplication.cdg.its.push({
            id: elem['Service Id']
          });
        }
      }
    });

    if (this.cdgArray === undefined) {
      this.cdgArray = [];
    }
    this.cdgArray.push(cdgApplication);

    this.addApplicationLayerIntraDeps(cdgApplication);
    this.addApplicationLayerInterDeps(cdgApplication);
  }
  addApplicationLayerIntraDeps(cdgApplication) {
    cdgApplication.cdg.its.forEach(elem => {
      if (elem.its === undefined) {
        const itdepArray: M[] = [];
        elem.itsdep = itdepArray;
      }
      this.serviceProfileData.forEach(line => {
        if (
          line['Service Id'] === elem.id &&
          line['Supported Asset Id'] !== '' &&
          this.getLayer(line['Supported Asset Type']) === 'ApplicationLayer' &&
          line['Supported Asset Type'] === 'IT Service'
        ) {
          elem.itsdep.push({
            id: line['Supported Asset Id'],
            weight: Weight[line['Supported Asset Weight']]
          });
        }

        if (
          line['Service Id'] === elem.id &&
          line['Supported Service Id'] !== '' &&
          this.getLayer(line['Supported Service Type']) ===
            'ApplicationLayer' &&
          line['Supported Service Type'] === 'IT Service'
        ) {
          elem.itsdep.push({
            id: line['Supported Service Id'],
            weight: Weight[line['Supported Service Weight']]
          });
        }
      });
      if (elem.itsdep.length < 1) {
        delete elem.itsdep;
      }
    });
  }
  addApplicationLayerInterDeps(cdgApplication: CDGS) {
    this.serviceProfileData.forEach(elem => {
      if (cdgApplication.cdg.ild === undefined) {
        const ildArray: Ild[] = [];
        cdgApplication.cdg.ild = ildArray;
      }
      if (elem['Supported Asset Id'] !== '') {
        const fromLayer = this.getLayer(elem.Type);
        const toLayer = this.getLayer(elem['Supported Asset Type']);
        if (
          fromLayer === 'ApplicationLayer' &&
          toLayer !== 'ApplicationLayer'
        ) {
          cdgApplication.cdg.ild.push({
            fromVertexId: elem['Service Id'],
            toVertexId: elem['Supported Asset Id'],
            toCdg: ToCdg[toLayer],
            weight: Weight[elem['Supported Asset Weight']]
          });
        }
      }

      if (elem['Supported Service Id'] !== '') {
        const fromLayer = this.getLayer(elem.Type);
        const toLayer = this.getLayer(elem['Supported Service Type']);
        if (
          fromLayer === 'ApplicationLayer' &&
          toLayer !== 'ApplicationLayer'
        ) {
          cdgApplication.cdg.ild.push({
            fromVertexId: elem['Service Id'],
            toVertexId: elem['Supported Service Id'],
            toCdg: ToCdg[toLayer],
            weight: Weight[elem['Supported Service Weight']]
          });
        }
      }
    });
    if (cdgApplication.cdg.ild.length < 1) {
      delete cdgApplication.cdg.ild;
    }
  }
  downloadFile() {
    if (!this.fileForDownload.includes('.')) {
      const tmpFileName = this.fileForDownload + '.cdgs';
      this.fileForDownload = tmpFileName;
      const jsonString = JSON.stringify(this.cdgArray);
      const file = new File([jsonString], this.fileForDownload, {
        type: 'text/plain;charset=utf-8'
      });
      FileSaver.saveAs(file);
    } else {
      const tmpFileName = this.fileForDownload.substr(
        0,
        this.fileForDownload.indexOf('.')
      );
      this.fileForDownload = tmpFileName + '.cdgs';
      const jsonString = JSON.stringify(this.cdgArray);
      const file = new File([jsonString], this.fileForDownload, {
        type: 'text/plain;charset=utf-8'
      });
      FileSaver.saveAs(file);
    }
  }
  // downloadCsvFileTemplates() {
  //   const link = document.createElement('a');
  //   link.download = 'csvFileTemplates.zip';
  //   link.href = 'assets/csvFileTemplates.zip';
  //   link.click();
  // }
}
